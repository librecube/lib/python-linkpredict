import unittest
import linkpredict as lp


class BasicTestCase(unittest.TestCase):

    def test_receiver(self):
        # Transmitter
        cables = lp.Device(gain=-1.315)
        connectors = lp.Device(gain=-0.3)
        filters = lp.Device(gain=-1)
        others = lp.Device(gain=-0.5)
        antenna_mismatch = lp.Device(gain=-0.5)
        transmitter = lp.Transmitter(
                amplifier_power=10, devices=[
                    cables, connectors, filters, others, antenna_mismatch])
        tx_antenna = lp.MainLobeAntenna(peak_gain=18.5, beam_3db_width=24)

        # Path
        polarization_loss = lp.SimpleMediumLoss(0.2)
        atmospheric_loss = lp.SimpleMediumLoss(2.1)
        ionospheric_loss = lp.SimpleMediumLoss(0.7)
        rain_loss = lp.SimpleMediumLoss(0)
        medium_losses = [
            polarization_loss, atmospheric_loss, ionospheric_loss, rain_loss]
        slant_range = 2783.87e3
        geometry = lp.SimpleGeometry(
            slant_range=slant_range,
            tx_antenna_angle=5,
            rx_antenna_angle=95.7)  # 95.7 instead of 20 to get 4.7 dB loss

        # Channel
        modulation = lp.FSKNonCoherentNoCoding(
            bit_rate=9600, bit_error_rate=1e-4, implementation_loss=1)
        channel = lp.Channel(frequency=145.8e6, modulation=modulation)

        # Receiver
        line_losses = lp.Device(gain=-1.96)
        receiver = lp.Receiver(devices=[line_losses])
        rx_antenna = lp.MainLobeAntenna(peak_gain=2.15, beam_3db_width=156.2)
        rx_antenna_noise = lp.SimpleAntennaNoise(261)

        # Link
        link = lp.Link(
            channel=channel,
            geometry=geometry,
            transmitter=transmitter,
            transmit_antenna=tx_antenna,
            medium_losses=medium_losses,
            receive_antenna=rx_antenna,
            receive_antenna_noise=rx_antenna_noise,
            receiver=receiver,
            )

        result = link.calculate_link_budget()
        k = lp.LinkBudgetKeys
        assert 24 < result[k.eirp] < 25

    def test_g_t_figure(self):
        # Transmitter
        cables = lp.Device(gain=-1.315)
        connectors = lp.Device(gain=-0.3)
        filters = lp.Device(gain=-1)
        others = lp.Device(gain=-0.5)
        antenna_mismatch = lp.Device(gain=-0.5)
        transmitter = lp.Transmitter(
                amplifier_power=10, devices=[
                    cables, connectors, filters, others, antenna_mismatch])
        tx_antenna = lp.MainLobeAntenna(peak_gain=18.5, beam_3db_width=24)

        # Path
        polarization_loss = lp.SimpleMediumLoss(0.2)
        atmospheric_loss = lp.SimpleMediumLoss(2.1)
        ionospheric_loss = lp.SimpleMediumLoss(0.7)
        rain_loss = lp.SimpleMediumLoss(0)
        medium_losses = [
            polarization_loss, atmospheric_loss, ionospheric_loss, rain_loss]
        slant_range = 2783.87e3
        geometry = lp.SimpleGeometry(
            slant_range=slant_range,
            tx_antenna_angle=5,
            rx_antenna_angle=95.7)  # 95.7 instead of 20 to get 4.7 dB loss

        # Channel
        modulation = lp.FSKNonCoherentNoCoding(
            bit_rate=9600, bit_error_rate=1e-4, implementation_loss=1)
        channel = lp.Channel(frequency=145.8e6, modulation=modulation)

        # Link
        link = lp.Link.from_g_t_figure(
            channel=channel,
            geometry=geometry,
            transmitter=transmitter,
            transmit_antenna=tx_antenna,
            medium_losses=medium_losses,
            rx_g_t_figure=-24,
            is_rx_linear_polarized=False,
            rx_antenna_pointing_loss=4.7
            )

        result = link.calculate_link_budget()
        k = lp.LinkBudgetKeys
        assert 24 < result[k.eirp] < 25


if __name__ == '__main__':
    unittest.main()
